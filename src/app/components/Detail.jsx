import React from 'react'
import {connect} from 'react-redux'
import { withRouter } from 'react-router'
import {Collapse} from 'react-collapse'

class Detail extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      tagline: '',
      description: '',
      image: '',
      brewers_tips: '',
      ibu: '',
      abv: '',
      ebc: '',
      ph: '',
      firstBrewed: '',
      foodPairings: [],
      isOpened: false,
      buttonText: 'Show more',
      similars: []
    }
  }
  // This method is launched when user clicks <ListItem/> or type in 
  // path for the first time (when component renders first time).
  componentDidMount () {

    if (this.props.id !== -1 && this.props.id > 0) {
      let abv, ibu, ebc
      // If beer is already loaded in reducer, get it from there, 
      // if not - fetch single beer to display for user.
      if (this.props.id < this.props.list.length) {
        abv = this.props.list[this.props.id - 1].abv
        ibu = this.props.list[this.props.id - 1].ibu
        ebc = this.props.list[this.props.id - 1].ebc
        this.fetchSimilarBeers(abv, ibu, ebc)
        this.setState({
          name: this.props.list[this.props.id - 1].name,
          tagline: this.props.list[this.props.id - 1].tagline,
          description: this.props.list[this.props.id - 1].description,
          brewers_tips: this.props.list[this.props.id - 1].brewers_tips,
          ibu: this.props.list[this.props.id - 1].ibu,
          abv: this.props.list[this.props.id - 1].abv,
          image: this.props.list[this.props.id - 1].image_url,
          ebc: this.props.list[this.props.id - 1].ebc,
          ph: this.props.list[this.props.id - 1].ph,
          firstBrewed: this.props.list[this.props.id - 1].first_brewed,
          foodPairings: this.props.list[this.props.id - 1].food_pairing
        })
      } else {
        fetch(`https://api.punkapi.com/v2/beers/${this.props.id}`)
        .then((response) => response.json())
        .then((json) => {
          this.setState({
            name: json[0].name,
            tagline: json[0].tagline,
            description: json[0].description,
            brewers_tips: json[0].brewers_tips,
            ibu: json[0].ibu,
            abv: json[0].abv,
            image: json[0].image_url,
            ebc: json[0].ebc,
            ph: json[0].ph,
            firstBrewed: json[0].first_brewed,
            foodPairings: json[0].food_pairing
          })
          abv = json[0].abv,
          ibu = json[0].ibu,
          ebc = json[0].ebc,
          this.fetchSimilarBeers(abv, ibu, ebc)
        })
      }
    }
  }

  // This method is launched when user manually type the path with :id - only 
  // when the <Modal/> is opened (to re-render the component)
  componentWillReceiveProps (nextProps) {
    if (nextProps.id !== -1 && nextProps.id > 0) {
      let abv, ibu, ebc 
      if (nextProps.id < this.props.list.length) {
        abv = nextProps.list[nextProps.id - 1].abv
        ibu = nextProps.list[nextProps.id - 1].ibu
        ebc = nextProps.list[nextProps.id - 1].ebc
        this.fetchSimilarBeers(abv, ibu, ebc)
        this.setState({
          name: nextProps.list[nextProps.id - 1].name,
          tagline: nextProps.list[nextProps.id - 1].tagline,
          description: nextProps.list[nextProps.id - 1].description,
          brewers_tips: nextProps.list[nextProps.id - 1].brewers_tips,
          ibu: nextProps.list[nextProps.id - 1].ibu,
          abv: nextProps.list[nextProps.id - 1].abv,
          image: nextProps.list[nextProps.id - 1].image_url,
          ebc: nextProps.list[nextProps.id - 1].ebc,
          ph: nextProps.list[nextProps.id - 1].ph,
          firstBrewed: nextProps.list[nextProps.id - 1].first_brewed,
          foodPairings: nextProps.list[nextProps.id - 1].food_pairing
        })
      } else {
        fetch(`https://api.punkapi.com/v2/beers/${nextProps.id}`)
        .then((response) => response.json())
        .then((json) => {
          this.setState({
            name: json[0].name,
            tagline: json[0].tagline,
            description: json[0].description,
            brewers_tips: json[0].brewers_tips,
            ibu: json[0].ibu,
            abv: json[0].abv,
            image: json[0].image_url,
            ebc: json[0].ebc,
            ph: json[0].ph,
            firstBrewed: json[0].first_brewed,
            foodPairings: json[0].food_pairing
          })
          abv = json[0].abv,
          ibu = json[0].ibu,
          ebc = json[0].ebc,
          this.fetchSimilarBeers(abv, ibu, ebc)
        })
      }
    }
  }

  handleExpand = () => {
    if(this.state.isOpened === true) {
      this.setState({
        isOpened: false,
        buttonText: 'Show more'
      })
    } else {
      this.setState({
        isOpened: true,
        buttonText: 'Hide more'
      })
    }
  }
  
  fetchSimilarBeers = (abv, ibu, ebc) => {
    const SIMILARITY_ABV = 1.5
    const SIMILARITY_IBU = ibu * 0.3
    let similarityEbcFloor
    let similarityEbcCeiling
    // Check if there is EBC index availble (some beers got null)
    if(ebc) {
      switch(true) {
        case ebc < 20: 
          similarityEbcFloor = ebc
          similarityEbcCeiling = ebc * 0.9
          break
        case ebc < 50: 
          similarityEbcFloor = ebc * 0.7
          similarityEbcCeiling = ebc * 0.7
          break
        case ebc < 100: 
          similarityEbcFloor = ebc * 0.6
          similarityEbcCeiling = ebc * 0.6
          break
        case ebc < 200: 
          similarityEbcFloor = ebc * 0.5
          similarityEbcCeiling = ebc * 0.5
          break
        default: 
          similarityEbcFloor = ebc * 0.3
          similarityEbcCeiling = ebc * 0.3
      }
    } else {
      similarityEbcFloor = ebc
      similarityEbcCeiling = 10
    }
    fetch(`https://api.punkapi.com/v2/beers?abv_gt=${Math.round(abv-SIMILARITY_ABV)}&abv_lt=${Math.round(abv+SIMILARITY_ABV)}&ibu_gt=${Math.round(ibu-SIMILARITY_IBU)}&ibu_lt=${Math.round(ibu+SIMILARITY_IBU)}&ebc_gt=${Math.round(ebc-similarityEbcFloor)}&ebc_lt=${Math.round(ebc+similarityEbcCeiling)}`)
    .then((response) => response.json())
    .then((json) => {
      this.setState({
        // Filter out displayed beer so it won't go to similars
        similars: json.filter((item) => {
          return item.id !== this.props.id
        })
      })
    })
  }

  renderSimilars = (count) => {
    const { similars } = this.state
    let twoSimilars, threeSimilars

    if(similars.length !== 0) {
      let arr = []
      let counter = 3

      // If there are too few similar beers (less than 3) then set counter to similar array lenght.
      if(similars.length < 3) {
        counter = similars.length
      }

      // Generate unique id's for similar beers to display
      while(arr.length < counter){
        let randomId = Math.round(Math.random() * (similars.length - 1));
        if(arr.indexOf(similars[randomId]) > -1) continue;
        arr.push(similars[randomId]);
      }
      return (
        <div>
          {arr.map((item, index) => {
            if(index < count) {
              return (
                <div key={item.id} className='modal__similar--item'>
                  <div><img onLoad={this.handleLoadImage} src={item.image_url}/></div>
                  <p>{item.name}</p>
                </div>
              )
            } else {
              return null
            }
          })}
        </div>
      )
    }
  }

  render () {
    if (this.props.id > 0 && this.props.id < 235) {
      return (
        <div className='modal__wrapper'>
          <div className='modal__main'>
            <div className='modal__upper'>
              <div className='modal__left'>
                <div className='modal__left--img'><img src={this.state.image} /></div>
              </div>
              <div className='modal__right'>
                <h1 className='modal__right--name'>{this.state.name}</h1>
                <h2 className='modal__right--tagline'>{this.state.tagline}</h2>
                <div className='modal__right--values'>
                  <p><span>IBU: </span>{this.state.ibu}</p>
                  <p><span>ABV: </span>{this.state.abv}%</p>
                </div>
                <p className='modal__right--description'>{this.state.description}</p>
              </div>
            </div>
            <div className='modal__more' onClick={this.handleExpand}>
              <div className='modal__more--button'>{this.state.buttonText}</div>
            </div>
            <Collapse isOpened={this.state.isOpened} forceInitialAnimation={true}>
              <div className='more'>
                <div className='more__top'>
                  <p>First brewed: {this.state.firstBrewed}</p>
                  <p>EBC: {this.state.ebc}</p>
                  <p>pH: {this.state.ph}</p>
                  <div className='more__top--food'>
                    <h1>Try with </h1>
                    <ul>
                      {this.state.foodPairings && this.state.foodPairings.map((item, index) => {
                        return (
                          <li key={index}>{item}</li> 
                        )
                      })}
                    </ul>
                  </div>
                </div>
                <div className='more__bottom'>
                  <h1>Brewers tips </h1>
                  <p className='more__bottom--tips'>{this.state.brewers_tips}</p>
                </div>
              </div>
            </Collapse>
          </div>
          <div className='modal__similar'>
            <h1 className='modal__similar--title'>You might also like:</h1>
            {this.renderSimilars(this.state.isOpened ? 3 : 2)}     
          </div>
        </div>
      )
    } else {
      return (
        <div> 
          <p>There is no beer here :( </p>
          <p>Probably wrong path or ID.</p>
        </div>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    list: state.mainReducer.beerList
  }
}

export default withRouter(connect(mapStateToProps, {})(Detail))
