import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import Router from './Router.jsx'
import store from './redux/store.js'

document.addEventListener('DOMContentLoaded', function () {
  ReactDOM.render(
    <Provider store={store}>
      <Router />
    </Provider>, document.getElementById('root'))
})
