import { createStore, combineReducers, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'

import mainReducer from './reducers/reducer.js'

export default createStore(
 combineReducers({mainReducer}),
 {},
 applyMiddleware(createLogger(), thunk)
)
