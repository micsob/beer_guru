import React from 'react'
import {connect} from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom';
import Modal from 'react-responsive-modal'
import Loader from 'react-loaders'

import InfiniteScroll from 'react-infinite-scroller'
import { isEmpty } from 'lodash'

import { fetchBeerList } from '../redux/actions/actions.js'

import Detail from './Detail.jsx'
import ListItem from './ListItem.jsx'


const spinner = () => {
  return (
    <div className="spinner">
      <div className="bounce1"></div>
      <div className="bounce2"></div>
      <div className="bounce3"></div>
    </div>
  )
}

class List extends React.Component {
  constructor (props) {
    super(props)
    this.isLoading = false
    this.state = {
      isModalOpen: false
    }
  }

  componentDidMount () {
    if(this.props.list.length === 0) {
      this.fetchBeers()
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.list.length !== this.props.list.length && !nextProps.allBeersLoaded && !nextProps.error) {
      this.isLoading = false
    }

    if(!this.state.isModalOpen && nextProps.location.pathname.includes('/details')) {
      this.setState({
        isModalOpen: true
      })
    }
  }

  fetchBeers = () => {
    if (!this.isLoading && !this.props.allBeersLoaded && !this.props.error) {
      this.isLoading = true
      this.props.fetchBeerList(this.props.page)
    }
  }

  getIdFromPath = () => {
    const path = this.props.location.pathname
    const lastSlashIndex = path.lastIndexOf('/') + 1
    let id = parseInt(path.substr(lastSlashIndex), 10)
    if(Number.isInteger(id)) {
      return id
    } else {
      return -1
    }

  }

  handleCloseModal = () => {
    this.setState({
      isModalOpen: false
    })
    this.props.history.push('/list')
  }

  render () {
    const classNames = { 
      overlay: 'modal__overlay',
      modal: 'modal__content',
    }

    if (!isEmpty(this.props.list)) {
      return (
        <div className='container'>
          <div className='row'>
            <InfiniteScroll
              pageStart={0}
              loadMore={this.fetchBeers}
              hasMore={!this.props.allBeersLoaded}
              loader={spinner()}
              >
                {this.props.list.map((item, index) => {
                  return (
                    <Link key={index} to={`/list/details/${this.props.list[index].id}`}>
                      <ListItem item={item} />
                    </Link>
                  )
                })}
            </InfiniteScroll>
            <Modal open={this.state.isModalOpen} onClose={this.handleCloseModal} showCloseIcon={false}  classNames={classNames}>
              <Detail id={this.getIdFromPath()}/>
            </Modal>
            {this.props.allBeersLoaded && <div className='load-info'>There are no more beers to load.</div>}
            {this.props.error && <div>{this.props.error}</div>}
          </div>
          
        </div>
      )
    } else {
      return <div>{this.props.error}</div>
      // dodać loader
    }
  }
}

const mapStateToProps = (state) => {
  return {
    list: state.mainReducer.beerList,
    allBeersLoaded: state.mainReducer.allBeersLoaded,
    error: state.mainReducer.error,
    page: state.mainReducer.page
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchBeerList: (page) => {
      dispatch(fetchBeerList(page))
    }
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(List))
