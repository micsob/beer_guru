import React from 'react'

export default class Header extends React.Component {
  render () {
    return (
      <header className='header'>
        <div className='container'>
          <div className='col-12 logo'>
            BeerGuru
          </div>
        </div>
      </header>
    )
  }
}
