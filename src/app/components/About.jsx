import React from 'react'

export default class About extends React.Component {
  render () {
    return (
      <div className='about'>
        <h1 className='about__title'>This is BeerGuru App. An application for Netguru recruitment process.</h1>
        <ol className='about__list'>Notes:
          <li>There are few things to notice:
            <ul>
              <li>Loaders on main list will appear for a split second even when image is loaded (when entered on list route without refreshing app).</li>
              <li>Loading of the main list is always delayed by 0.5 second just to see the bottom loader of infinte scroller easier.</li>
              <li>Also somehow my loaders in modals didn't work properly so I removed them.</li>
              <li>In modal, 'Show more' section open too fast (instead of smooth animation) because of 3rd similar beer rendering(no time to fix - not very important). It looks good on mobile though.</li>
              <li>After closing main modal, there is that small modal for a split second I cannot get rid of. </li>
            </ul>
          </li>
          <li>As we are in development environment I left redux-logger running (should be turned off for production).</li>
          <li>Unfortunately, I could not apply any tests to my code. Hope it would not matter that much in this case.</li>
          <li>Excuse my UI/UX taste, I am not good designer :(</li>

        </ol>
      </div>
    )
  }
}
