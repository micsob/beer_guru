import React from 'react'

export default class NoMatch extends React.Component {
  render () {
    return (
      <div className='no-match'>
        <p>404 Beer Not Found :( </p>
        <p>You probably entered wrong url. </p>
      </div>
    )
  }
}
