import React from 'react'
import { NavLink } from 'react-router-dom'

export default class MainButton extends React.Component {
  render () {
    return (
      <NavLink to={this.props.path} className='main__button'><p>{this.props.name}</p></NavLink>
    )
  }
}
