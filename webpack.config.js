const webpack = require('webpack')
const path = require('path')

module.exports = {
  devtool: 'cheap-eval-source-map',
  entry: './src/app/index.jsx',
  output: {
    path: path.resolve(__dirname, './src/dist'),
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: './src/public'
  },
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          query: {
            compact: true,
            presets: ['react', 'env', 'stage-2']
          }
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          query: {
            compact: true,
            presets: ['env', 'stage-2']
          }
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.(woff|woff2)$/,
        loader: 'file-loader?name=fonts/[name].[ext]'
      },
      {
        test: /\.scss$/,
        use: [ 'style-loader', 'css-loader', 'postcss-loader', 'sass-loader' ]
      },
      {
        test: /\.(jpg|png|jpe?g|gif|svg|ico)$/,
        use: 'file-loader?name=images/[name].[ext]'
      }
    ]
  }
}
