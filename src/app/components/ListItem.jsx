import React from 'react'

import Loader from 'react-loaders'

export default class ListItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      bgColor: '',
      transform: '',
      transition: '0.5s',
      zIndex: '',
      position: '',
      padding: '',
      paddingName: '',
      loading: true
    }
  }

  handleMouseEnter = () => {
    this.setState({
      bgColor: 'rgba(255,255,255,1)',
      transform: 'scale(1.07)',
      zIndex: 2,
      position: 'relative',
      padding: 0,
      paddingName: '15px'
    })
  }
  handleMouseLeave = () => {
    this.setState({
      bgColor: 'rgba(255,255,255,1)',
      transform: 'scale(1)',
      zIndex: 0,
      position: 'static',
      padding: '10px',
      paddingName: 0
    })
  }

  handleLoadImage = () => {
    this.setState({
      loading: false
    })
  }
  render () {
    const styleDiv = {
      backgroundColor: this.state.bgColor,
      transform: this.state.transform,
      transition: this.state.transition,
      zIndex: this.state.zIndex,
      position: this.state.position,
      padding: this.state.padding
    }

    const { name, image_url, tagline } = this.props.item
    
    return (
      <div 
        onMouseEnter={this.handleMouseEnter} 
        onMouseLeave={this.handleMouseLeave} 
        style={styleDiv} 
        className='list__item col-3'
        >
        <div className='list__item--inner'>
          <Loader style={{transform: 'scale(2)'}} type="ball-clip-rotate-multiple" active={this.state.loading}/>
          <h1 style={{paddingTop: this.state.paddingName}} className='list__item--name'>{name}</h1>
          <img onLoad={this.handleLoadImage} className='list__item--img' src={image_url} />
          <h2 className='list__item--tagline'>{tagline}</h2>
        </div>
      </div>
    )
  }
}
