const mainReducer = (state = {
  beerList: [],
  allBeersLoaded: false,
  error: null,
  page: 1
}, action) => {
  switch (action.type) {
    case 'GET_BEER_LIST':
      state = {
        ...state,
        allBeersLoaded: action.payload.length === 0,
        beerList: state.beerList.concat(action.payload),
        page: action.page + 1
      }
      break
    case 'ERROR':
      state = {
        ...state,
        error: action.payload
      }
      break
    default:
      return state
  }
  return state
}

export default mainReducer
