import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { ModalContainer } from 'react-router-modal'
import { ModalRoute } from 'react-router-modal'

import About from './About.jsx'
import List from './List.jsx'
import NoMatch from './NoMatch.jsx'
import Detail from './Detail.jsx'
import MainButton from './MainButton.jsx'

export default class Main extends React.Component {
  render () {
    return (
      <main className='container'>
        <nav className='main__navigation'>
          <MainButton name={'About'} path={'/about'} />
          <MainButton name={'Beer List'} path={'/list'} />
        </nav>
        <div className='main__wrapper'>
          <Switch>
            <Route exact path='/' component={About} />
            <Route path='/about' component={About} />
            <Route path='/list' component={List} />
            <ModalRoute path='/details/:id' component={Detail} />
            <Route path='*' component={NoMatch} />
          </Switch>
          <ModalContainer />
        </div>
      </main>
    )
  }
}
