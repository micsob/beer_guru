export function fetchBeerList (page) {
  // Timeout to make loading slower, so one can actually see the loader
  return dispatch => {
    setTimeout(() => {
      fetch(`https://api.punkapi.com/v2/beers?page=${page}&per_page=20`)
      .then((response) => response.json())
      .then((json) => {
        if (!json.message) {
          dispatch({
            type: 'GET_BEER_LIST',
            payload: json,
            page
          })
        } else {
          dispatch({
            type: 'ERROR',
            payload: json.message
          })
        }
      })
      .catch((err) => dispatch({
        type: 'ERROR',
        payload: err
      }))
    }, 500)
  }
}
