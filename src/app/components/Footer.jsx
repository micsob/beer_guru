import React from 'react'

export default class Footer extends React.Component {
  render () {
    return (
      <footer className='footer'>
        <div className='container'>
          <div className='col-12 footer__inner'>
            <div>BeerGuru app v1 by <span>Michał Sobczak</span></div>
            <div className='icons'>
              <a href='https://github.com/merio901' target='_blank'><i className='fa fa-github' aria-hidden='true' /></a>
              <a href='https://www.linkedin.com/in/micha%C5%82-sobczak-13035b149/' target='_blank'><i className='fa fa-linkedin' aria-hidden='true' /></a>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}
