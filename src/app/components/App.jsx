import React from 'react'

import '../../styles/main.scss'

import Header from './Header.jsx'
import Main from './Main.jsx'
import Footer from './Footer.jsx'

export default class App extends React.Component {
  render () {
    return (
      <div className='container__wrapper'>
        <Header />
        <Main />
        <Footer />
      </div>
    )
  }
}
