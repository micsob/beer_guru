import React from 'react'
import { HashRouter } from 'react-router-dom'

import App from './components/App.jsx'

const Router = () => {
  return (
    <HashRouter>
      <App />
    </HashRouter>
  )
}

export default Router
